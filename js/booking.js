function setEventGroupDownMovie() {
  var movieGroup = document.querySelectorAll("#movieGroup .option");
  var optionGroup = document.querySelector("#movieGroup .optionGroup");

  for (let i = 0; i < movieGroup.length; i++) {
    const moive = movieGroup[i];
    const value = moive.textContent;
    var firstDay = document.getElementById("day_2");

    moive.onclick = function() {
      setTextMoiveActive(value);
      // Đóng groupdown
      optionGroup.style.maxHeight = "0px";
      optionGroup.style.transition = "0.3s all";

      clearClassActiveDay();
      firstDay.classList.add("dayActive");
      showGroupTime(firstDay.id);

      setTimeout(() => {
        optionGroup.style.maxHeight = "";
        optionGroup.style.transition = "";
      }, 1000);
      reloadEvent();
      clearInfoBooking();
    };
  }
}

function setTextMoiveActive(text) {
  var optionChosen = document.querySelector("#movieGroup .optionChosen");
  optionChosen.innerHTML = text + '<i class="fas fa-angle-down"></i>';
}

function setEventClickDay() {
  var days = document.querySelectorAll(".selectDay .day");

  for (let i = 0; i < days.length; i++) {
    const day = days[i];
    day.onclick = function() {
      clearClassActiveDay();
      day.classList.add("dayActive");
      showGroupTime(day.id);
      reloadEvent();
    };
  }
}

function showGroupTime(idDay) {
  var idTime = "time_" + idDay.split("_")[1]; //idDay : day_2 ,day_3, ...
  var time = document.getElementById(idTime);
  clearClassShowGroupTime();
  time.classList.add("showGroupTime");
}

function clearClassShowGroupTime() {
  var allGroupTime = document.querySelectorAll(".selectTime .groupTime");
  for (let i = 0; i < allGroupTime.length; i++) {
    const groupTime = allGroupTime[i];
    groupTime.classList.remove("showGroupTime");
  }
}

function clearClassActiveDay() {
  var allDayInGroup = document.querySelectorAll(".selectDay .day");

  for (let i = 0; i < allDayInGroup.length; i++) {
    const day = allDayInGroup[i];
    day.classList.remove("dayActive");
  }
}

function setEventClickTime() {
  var times = document.querySelectorAll(".selectTime .time");
  for (let i = 0; i < times.length; i++) {
    const time = times[i];
    if (!time.classList.contains("hide")) {
      time.onclick = function() {
        clearClassActiveTime(time);
        time.classList.add("activeTime");
        reloadEvent();
      };
    }
  }
}

function clearClassActiveTime(_this) {
  var allTimeInGroup = _this.parentNode.children;

  for (let i = 0; i < allTimeInGroup.length; i++) {
    const time = allTimeInGroup[i];
    time.classList.remove("activeTime");
  }
}

function setSeat(numberOfSeat, numberOfCouple) {
  var room = document.getElementById("room");
  if (room.childNodes.length > 0) {
    room.innerHTML = "";
  }
  var dataBooked = genarateDataBooked(numberOfSeat, numberOfCouple, 40);
  var dataVip = [100, 160];
  var dataCouple = [numberOfSeat - numberOfCouple + 1, numberOfSeat];
  var countSeatInRow = 0; // countOfRow <= 20
  var numberOfRow =
    numberOfSeat % 20 === 0
      ? (numberOfSeat + numberOfCouple) / 20
      : parseInt((numberOfSeat + numberOfCouple) / 20) + 1;
  console.log();
  var currentRow = 0;
  var alphabet = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z"
  ];

  genarateRow(numberOfRow);
  for (let i = 0; i < numberOfSeat; i++) {
    // Ghế trống
    var nodeSeat = document.createElement("i");
    nodeSeat.classList = "fas fa-couch";

    // Ghế đã đặt
    var nodeSeatBooked = document.createElement("i");
    nodeSeatBooked.classList = "fas fa-couch booked";

    // Ghế vip
    var nodeSeatVip = document.createElement("i");
    nodeSeatVip.classList = "fas fa-couch vip";

    // Ghế đôi
    var nodeSeatCouple = document.createElement("i");
    nodeSeatCouple.classList = "fas fa-couch couple";

    if (countSeatInRow >= 20) {
      currentRow++;
      countSeatInRow = 0;
    }
    var id = `${alphabet[currentRow]}`;
    // var row = document.getElementById(id);
    var left = document.querySelector(`#${id} .left`);
    var center = document.querySelector(`#${id} .center`);
    var right = document.querySelector(`#${id} .right`);
    var curNodeSeat = nodeSeat;
    curNodeSeat.id = `${id + (countSeatInRow + 1)}_65`;
    var stateCouple = false;

    if (i + 1 >= dataVip[0] && i + 1 <= dataVip[1]) {
      curNodeSeat = nodeSeatVip.cloneNode(true);
      curNodeSeat.id = `${id + (countSeatInRow + 1)}_100`;
    }

    if (i + 1 >= dataCouple[0] && i + 1 <= dataCouple[1]) {
      curNodeSeat = nodeSeatCouple.cloneNode(true);
      curNodeSeat.id = `${id + (countSeatInRow + 1)}_160`;
      stateCouple = true;
    }

    if (dataBooked.indexOf(i + 1) >= 0) {
      curNodeSeat = nodeSeatBooked.cloneNode(true);
    }

    if (
      (stateCouple && countSeatInRow === 4) ||
      (stateCouple && countSeatInRow === 14)
    )
      countSeatInRow++;

    if (countSeatInRow >= 15) {
      right.appendChild(curNodeSeat);
      countSeatInRow++;
      if (stateCouple) countSeatInRow++;
      continue;
    }

    if (countSeatInRow >= 5) {
      center.appendChild(curNodeSeat);
      countSeatInRow++;
      if (stateCouple) countSeatInRow++;
      continue;
    }

    if (countSeatInRow >= 0) {
      left.appendChild(curNodeSeat);
      countSeatInRow++;
      if (stateCouple) countSeatInRow++;
      continue;
    }
  }
}

function genarateRow(numberOfRow) {
  var room = document.getElementById("room");
  var alphabet = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z"
  ];
  for (let i = 0; i < numberOfRow; i++) {
    var id = alphabet[i];
    var row = document.createElement("div");
    row.id = `${id}`;
    row.className = "row";
    var label = document.createElement("label");
    label.textContent = `${id}`;
    var left = document.createElement("div");
    left.className = "left";
    var center = document.createElement("div");
    center.className = "center";
    var right = document.createElement("div");
    right.className = "right";

    row.appendChild(label);
    row.appendChild(left);
    row.appendChild(center);
    row.appendChild(right);

    room.appendChild(row);
  }
  // console.log(room);
}
// function getRandomNumber(max) {
//   return Math.floor(Math.random() * max) + 1 ;
// }
function genarateDataBooked(numberOfSeat, numberOfCouple, quatity) {
  var result = [];
  for (let i = 0; i < quatity; i++) {
    var number =
      Math.floor(Math.random() * (numberOfSeat - numberOfCouple)) + 1;
    if (result.indexOf(number) >= 0) {
      i--;
    } else {
      result.push(number);
    }
  }

  return result;
}

function getMovieName() {
  var optionChosen = document.querySelector("#movieGroup .optionChosen");
  return optionChosen.textContent;
}

function getTime() {
  var id = document.querySelector(".selectDay .groupDay .dayActive ").id;
  id = id.split("_")[1];
  var time = document.querySelector(`#time_${id} .activeTime`).textContent;
  return time;
}

function getDate() {
  var date = document.querySelectorAll(
    ".selectDay .groupDay .dayActive > span "
  )[1].textContent;
  return date + "/2019";
}

function getNumberSeat() {
  var result = "";
  var bookingSeat = document.querySelectorAll("#room .myBooking");
  for (let i = 0; i < bookingSeat.length; i++) {
    const seat = bookingSeat[i];
    if (i === bookingSeat.length - 1) {
      result = result + seat.id.split("_")[0];
      break;
    }

    result = result + seat.id.split("_")[0] + ", ";
  }
  return result;
}

function getQuatity() {
  var bookingSeat = document.querySelectorAll("#room .myBooking");
  return bookingSeat.length;
}

function getTotal() {
  var result = 0;
  var bookingSeat = document.querySelectorAll("#room .myBooking");
  for (let i = 0; i < bookingSeat.length; i++) {
    const seat = bookingSeat[i];

    result = result + parseInt(seat.id.split("_")[1]);
  }
  return result;
}

function updateInfoBooking() {
  var movieName = document.getElementById("movieName");
  var time = document.getElementById("time");
  var date = document.getElementById("date");
  var quatity = document.getElementById("quatity");
  var numberSeat = document.getElementById("numberSeat");
  var total = document.getElementById("total");
  movieName.innerHTML = getMovieName();
  time.innerHTML = getTime();
  date.innerHTML = getDate();
  quatity.innerHTML = getQuatity();
  numberSeat.innerHTML = getNumberSeat();
  if (getTotal() === 0) {
    total.innerHTML = getTotal() + "đ";
  } else total.innerHTML = getTotal() + ",000đ";
}

function clearInfoBooking() {
  var movieName = document.getElementById("movieName");
  var time = document.getElementById("time");
  var date = document.getElementById("date");
  var quatity = document.getElementById("quatity");
  var numberSeat = document.getElementById("numberSeat");
  var total = document.getElementById("total");
  movieName.innerHTML = getMovieName();
  time.innerHTML = ". . .";
  date.innerHTML = ". . .";
  quatity.innerHTML = ". . .";
  numberSeat.innerHTML = ". . .";
  total.innerHTML = ". . .";
}

function setEventBooking() {
  var allSeat = document.querySelectorAll("#room .fa-couch");
  for (let i = 0; i < allSeat.length; i++) {
    const seat = allSeat[i];
    if (!seat.classList.contains("booked")) {
      seat.onclick = function() {
        if (seat.classList.contains("myBooking")) {
          seat.classList.remove("myBooking");
        } else seat.classList.add("myBooking");

        updateInfoBooking();
      };
    }
  }
}
function reloadEvent() {
  setEventClickTime();
  setEventClickDay();
  setSeat(249, 9);
  setEventBooking();
}
//Hàm chạy khi khởi tạo
setEventGroupDownMovie();
reloadEvent();

function setDefaultTagA() {
  var day = document.querySelectorAll(".day a");
  for (let i = 0; i < day.length; i++) {
    day[i].onclick = function(e) {
      e.preventDefault();
    };
  }
}

var slideActive = 2;
// xử lý slider Chính
function show(id) {
  clearActiveDot();
  addActiveDot(id);
  setBackground(id);
  clearActiveItem();
  addActiveItem(id);
  setSlideActive(id + 1);
  clearInterval(idAuto);
  setTimeout(() => {
    autoSlide1();
  }, 5000);
}

// Xử lý auto slide chính
function showForAuto(id) {
  clearActiveDot();
  addActiveDot(id);
  setBackground(id);
  clearActiveItem();
  addActiveItem(id);
  slideActive = id;
}

function clearActiveDot() {
  var listDots = document.getElementsByClassName("dotSlides")[0].children;

  for (var i = 0; i < listDots.length; i++) {
    var classes = listDots[i].classList;

    for (let j = 0; j < classes.length; j++) {
      if (classes[j] === "activeDot") {
        classes.remove("activeDot");
      }
    }
  }
}

function addActiveDot(id) {
  var dot = document.getElementById("dot" + id);
  var classes = dot.classList;
  classes.add("activeDot");
}

function setBackground(id) {
  var background = document.getElementById("slideBackground");
  // var movies = document.getElementsByClassName("movie")[0];

  // background.children[0].classList.add("fadeBackground");
  // movies.classList.add("fadeBackground");
  // setTimeout(() => {
  //   // background.children[0].classList.remove("fadeBackground");
  //   movies.classList.remove("fadeBackground");
  // }, 1000);

  if (id === 1) {
    background.style.backgroundImage = "url(images/aladin.jpg)";
  }

  if (id === 2) {
    background.style.backgroundImage = "url(images/pikachu.jpg)";
  }

  if (id === 3) {
    background.style.backgroundImage = "url(images/endgame.jpg)";
  }
}

function addActiveItem(id) {
  var item = document.getElementById("item" + id);
  item.classList.add("activeItem");
}

function clearActiveItem() {
  var listItems = document.getElementsByClassName("movie")[0].children;

  for (var i = 0; i < listItems.length; i++) {
    var classes = listItems[i].classList;

    for (let j = 0; j < classes.length; j++) {
      if (classes[j] === "activeItem") {
        classes.remove("activeItem");
      }
    }
  }
}

function setSlideActive(id) {
  if (id > 3) {
    slideActive = 1;
  } else slideActive = id;
}

var idAuto = null;
function autoSlide1() {
  var time = 5000;
  idAuto = setInterval(() => {
    showForAuto(slideActive);
    setSlideActive(slideActive + 1);
  }, time);
}

function showFixedNav() {
  var fixedNav = document.getElementById("fixedNav");
  fixedNav.classList.remove("hideNav");

  var classes = fixedNav.classList;
  classes.add("showNav");
}

function hideFixedNav() {
  var fixedNav = document.getElementById("fixedNav");
  fixedNav.classList.remove("showNav");
  var classes = fixedNav.classList;
  classes.add("hideNav");
}

// Các hàm sẽ chạy
autoSlide1();
